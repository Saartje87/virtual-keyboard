(function (global, factory) {
	if (typeof define === "function" && define.amd) {
		define('VirtualKeyboard', ['module'], factory);
	} else if (typeof exports !== "undefined") {
		factory(module);
	} else {
		var mod = {
			exports: {}
		};
		factory(mod);
		global.VirtualKeyboard = mod.exports;
	}
})(this, function (module) {
	'use strict';

	function _classCallCheck(instance, Constructor) {
		if (!(instance instanceof Constructor)) {
			throw new TypeError("Cannot call a class as a function");
		}
	}

	var _createClass = function () {
		function defineProperties(target, props) {
			for (var i = 0; i < props.length; i++) {
				var descriptor = props[i];
				descriptor.enumerable = descriptor.enumerable || false;
				descriptor.configurable = true;
				if ("value" in descriptor) descriptor.writable = true;
				Object.defineProperty(target, descriptor.key, descriptor);
			}
		}

		return function (Constructor, protoProps, staticProps) {
			if (protoProps) defineProperties(Constructor.prototype, protoProps);
			if (staticProps) defineProperties(Constructor, staticProps);
			return Constructor;
		};
	}();

	function _renderRow(items) {
		return '<div class="virtual-keyboard-row">' + items + '</div>';
	}

	;

	function renderItem(data) {
		var item = specials[data] || {};
		return '<div class="virtual-keyboard-item ' + (item.flex && 'flex-' + item.flex || '') + '"\n\t\tvk-char="' + (item.action || data) + '">\n\t\t\t<div class="virtual-keyboard-item-inner">\n\t\t\t\t' + (item.display || data) + '\n\t\t\t</div>\n\t\t</div>';
	}

	;
	var layout = ['qwertyuiop', 'asdfghjkl', 'zxcvbnm˿', ' ¬'];
	var specials = {
		'˿': {
			flex: 2
		},
		' ': {
			flex: 2,
			display: '̺'
		},
		'¬': {
			display: 'Enter'
		}
	};

	var VirtualKeyboard = function () {
		function VirtualKeyboard(element, options) {
			_classCallCheck(this, VirtualKeyboard);

			this.value = '';
			this.element = element;
			this.options = options;
			this.render();
			this.addActions();
		}

		_createClass(VirtualKeyboard, [{
			key: 'addActions',
			value: function addActions() {
				var _this = this;

				var eventType = 'ontouchstart' in window ? 'touchstart' : 'click';
				var inputElement = this.options.inputElement;
				inputElement.addEventListener('focus', function (event) {
					_this.element.classList.add('virtual-keyboard-show');

					inputElement.classList.add('virtual-input-focus');
				});
				document.addEventListener('click', function (event) {
					if (inputElement.contains(event.target) || _this.element.contains(event.target)) {
						return;
					}

					_this.element.classList.remove('virtual-keyboard-show');

					inputElement.classList.remove('virtual-input-focus');
				});
				document.addEventListener(eventType, function (event) {
					var target = event.target;

					if (!target.parentNode || !target.parentNode.getAttribute) {
						return;
					}

					var value = target.getAttribute('vk-char') || target.parentNode.getAttribute('vk-char');
					var removeLastChild = false;
					var isHtml = false;

					if (!value) {
						return;
					}

					switch (value) {
						case '˿':
							_this.value = _this.value.substr(0, _this.value.length - 1);
							removeLastChild = true;
							break;

						case '¬':
							_this.value += value;
							value = '<br>';
							isHtml = true;
							break;

						case ' ':
							_this.value += value;
							value = '&nbsp;';
							isHtml = true;
							break;

						default:
							_this.value += value;
					}

					if (inputElement) {
						if (removeLastChild) {
							if (inputElement.lastElementChild) {
								inputElement.removeChild(inputElement.lastElementChild);
							}
						} else {
							(function () {
								var span = document.createElement('span');

								if (isHtml) {
									span.innerHTML = value;
								} else {
									span.textContent = value;
								}

								window.requestAnimationFrame(function () {
									inputElement.appendChild(span);
								});
							})();
						}
					}

					if (navigator.vibrate) {
						navigator.vibrate(15);
					}
				});
			}
		}, {
			key: 'render',
			value: function render() {
				var _this2 = this;

				var html = '';
				layout.forEach(function (row) {
					html += _this2.renderRow(row);
				});
				this.element.innerHTML = html;
			}
		}, {
			key: 'renderRow',
			value: function renderRow(row) {
				var items = row.split('').map(function (item) {
					return renderItem(item);
				}).join('');
				return _renderRow(items);
			}
		}]);

		return VirtualKeyboard;
	}();

	;
	module.exports = VirtualKeyboard;
});
//# sourceMappingURL=virtual-keyboard.js.map
