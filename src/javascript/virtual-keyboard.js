function renderRow (items) {
	return `<div class="virtual-keyboard-row">${items}</div>`;
};

function renderItem (data) {
	let item = specials[data] || {};

	return `<div class="virtual-keyboard-item ${item.flex && 'flex-'+item.flex || ''}"
		vk-char="${item.action || data}">
			<div class="virtual-keyboard-item-inner">
				${item.display || data}
			</div>
		</div>`;
};

const layout = [
	'qwertyuiop',
	'asdfghjkl',
	'zxcvbnm\u02FF',
	'\u0020\u00AC'
];

const specials = {
	// Backspace
	'\u02FF': {
		flex: 2
	},
	// Whitespace
	'\u0020': {
		flex: 2,
		display: '\u033A'
	},
	// Enter
	'\u00AC': {
		display: 'Enter',
		// action: '\u000A',
		// flex: 2
	}
};

export default class VirtualKeyboard {

	constructor (element, options) {
		this.value = '';
		this.element = element;
		this.options = options;

		this.render();

		this.addActions();
	}

	addActions () {
		let eventType = 'ontouchstart' in window ? 'touchstart' : 'click';
		let inputElement = this.options.inputElement;

		inputElement.addEventListener('focus', event => {
			this.element.classList.add('virtual-keyboard-show');
			inputElement.classList.add('virtual-input-focus');
		});

		document.addEventListener('click', event => {
			if (inputElement.contains(event.target) ||
				this.element.contains(event.target)) {
				return;
			}

			this.element.classList.remove('virtual-keyboard-show');
			inputElement.classList.remove('virtual-input-focus');
		});

		// this.element does not work as expected with touchstart??
		document.addEventListener(eventType, event => {
			let target = event.target;

			if(!target.parentNode || !target.parentNode.getAttribute) {
				return;
			}

			let value = target.getAttribute('vk-char') || target.parentNode.getAttribute('vk-char');
			let removeLastChild = false;
			let isHtml = false;

			if (!value) {
				return;
			}

			switch (value) {
				case '\u02FF':
					this.value = this.value.substr(0, this.value.length - 1);
					removeLastChild = true;
					break;
				case '\u00AC':
					this.value += value;
					value = '<br>';
					isHtml = true;
					break;
				case '\u0020':
					this.value += value;
					value = '&nbsp;';
					isHtml = true;
					break;
				default:
					this.value += value;
			}

			// Spaghetti ftw!
			if (inputElement) {
				if (removeLastChild) {
					if (inputElement.lastElementChild) {
						inputElement.removeChild(inputElement.lastElementChild);
					}
				} else {
					let span = document.createElement('span');

					if (isHtml) {
						span.innerHTML = value;
					} else {
						span.textContent = value;
					}

					window.requestAnimationFrame(() => {
						inputElement.appendChild(span);
					});
				}
			}

			if (navigator.vibrate) {
				navigator.vibrate(15);
			}
		});
	}

	render () {
		let html = '';

		layout.forEach(row => {
			html += this.renderRow(row);
		});

		this.element.innerHTML = html;
	}

	renderRow (row) {
		let items = row.split('').map(item => renderItem(item)).join('');

		return renderRow(items);
	}
};
