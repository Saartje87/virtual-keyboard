// Add tests

var gulp = require('gulp');
var rollup = require('gulp-rollup');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');

gulp.task('default', ['build-js', 'build-css']);
gulp.task('serve', ['watch', 'browser-sync']);
gulp.task('watch', ['build-js', 'build-css'], function () {

	gulp.watch('src/javascript/**/*.js', ['build-js']);
	gulp.watch('src/sass/**/*.scss', ['build-css']);
	// gulp.watch(['tests/**/*.js', 'dist/**/*.js'], ['test-js']);
});

gulp.task('build-js', function () {

	gulp.src('src/javascript/virtual-keyboard.js', {read: false})
		.pipe(rollup({
			moduleId: 'VirtualKeyboard',
			moduleName: 'VirtualKeyboard',
			format: 'cjs',
			sourceMap: true,
			dest: 'virtual-keyboard.js'
		})
			.on('error', function (err) {
				console.log(err);
			}))
		.pipe(babel({
				moduleId: 'VirtualKeyboard',
				presets: ['es2015'],
				plugins: ['transform-es2015-modules-umd']
			})
			.on('error', function (err) {
				console.log(err);
			}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('public/scripts'))
		.pipe(reload({stream: true})); ;
});

gulp.task('build-css', function () {

	gulp.src('src/sass/virtual-keyboard.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({browsers: ['last 3 versions']}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./public/css/'))
		.pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('browser-sync', function () {

	browserSync({
		server: './public',
		files: ['public/*.html'],
		host: 'niek',
		open: 'external'
	});
});
